# README #

* Pit game
* A simple mancala board game version using Java,Javascript,Websocket,Json,Jetty.

See a live version @ http://netplay.nl:8085

### How do I get set up? ###

 * Use maven mvn jetty:run to run a local (http://localhost:8080) copy of the game on the embedded jetty server.
 * Edit the websocket host in pitgame.js to "ws://localhost:8080/websocket" in orde to run on your local jetty server.