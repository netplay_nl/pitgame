package nl.netplay.pitgame.game;

import nl.netplay.pitgame.server.GameManager;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nieuwl on 25-9-2015.
 */
public class TestGameManager {

    private GameManager gameManager;
    private Speler speler1;
    private Speler speler2;
    private List<Speler> spelerList= new ArrayList<Speler>();

    @Test
    public void testMaakSpel() throws Exception {
        speler1 = new Speler("speler1", "test-sessie_1");
        speler2 = new Speler("speler2", "test-sessie_2");
        spelerList.add(speler1);
        spelerList.add(speler2);
        gameManager = GameManager.getInstance();

        //maak een standaard spel
        gameManager.maakSpel(spelerList,null);

        //spel moet bestaan
        Assert.assertEquals(1, gameManager.getGames().size());

        List<Game> games = gameManager.getGames();
        Game game = games.get(0);

        game.stopSpel(null);

    }

    @Test
    public void testStopSpel() throws Exception {
        speler1 = new Speler("speler1", "test-sessie_1");
        speler2 = new Speler("speler2", "test-sessie_2");
        spelerList.add(speler1);
        spelerList.add(speler2);
        gameManager = GameManager.getInstance();

        gameManager.maakSpel(spelerList,null);

        //spel moet bestaan
        Assert.assertEquals(1, gameManager.getGames().size());

        List<Game> games = gameManager.getGames();
        Game game = games.get(0);

        game.stopSpel(null);

        //spel moet niet meer bestaan
        Assert.assertEquals(0, gameManager.getGames().size());
    }

}
