package nl.netplay.pitgame.game;

import org.junit.Before;
import org.junit.Test;

import nl.netplay.pitgame.server.GameManager;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by leon on 20-9-2015.
 *
 * unit test voor het veroveren van pits in verschillende scenario's
 */
public class TestGameVeroverPit {

    private GameManager gameManager;
    private Game game;
    private Speler speler1;
    private Speler speler2;
    private List<Speler> spelerList= new ArrayList<Speler>();
    private Pit[] pitCollectie = new Pit[14];

    @Before
    public void setUp() {

        speler1 = new Speler("speler1", "test-sessie_1");
        speler2 = new Speler("speler2", "test-sessie_2");
        spelerList.add(speler1);
        spelerList.add(speler2);
        gameManager = GameManager.getInstance();
    }

    @Test
    public void testVeroverPit() {

        /**
         De test controleert meerdere regels met een "normale" verovering

        1) de laatste steen verovert de pit tegenover als die van de tegenstander is en de laatste pit leeg was
        2) de score pit wordt correct bijgewerkt
        3) de veroverde pits worden leeggemaakt

        speler1 kiest voor pit 1, die bevat 1 steen
        steen land in pit 1, die bevatte 0 stenen
        de laatste steen land in lege pit, dus verovert deze te tegenovergestelde pit 12
        pit 2 en 12 moeten 0 stenen bevatten
        pit 7 moet 14 stenen bevatten (scorepit 7 (4 stenen) + pit 2 (1 steen) + pit 12 (9 stenen) = 14 stenen)
        */

        //stel spel situatie waarbij pit veroverd kan worden
        //speler 1 pits
        pitCollectie[0] = new Pit(1,1);
        pitCollectie[1] = new Pit(2,0);
        pitCollectie[2] = new Pit(3,12);
        pitCollectie[3] = new Pit(4,9);
        pitCollectie[4] = new Pit(5,0);
        pitCollectie[5] = new Pit(6,0);
        //score pit speler 1
        pitCollectie[6] = new Pit(7,4);

        //speler 2 pits
        pitCollectie[7] = new Pit(8,0);
        pitCollectie[8] = new Pit(9,3);
        pitCollectie[9] = new Pit(10,11);
        pitCollectie[10] = new Pit(11,9);
        pitCollectie[11] = new Pit(12,9);
        pitCollectie[12] = new Pit(13,0);
        //score pit speler 2
        pitCollectie[13] = new Pit(14,14);

        gameManager.maakSpel(spelerList,pitCollectie);

        List<Game> games = gameManager.getGames();
        game = games.get(0);

        //speler2 is aan zet
        speler1.setAanDeBeurt(true);
        speler2.setAanDeBeurt(false);

        //speler1 kiest pit 2
        game.behandelSpelerZet(1, "test-sessie_1");

        //pit 8 moet waarde 0 bevatten, die wordt leeggemaakt omdat er veroverd wordt
        assertEquals(0, pitCollectie[1].getAantalStenen());
        //pit 6 moet waarde 0 bevatten, die wordt leeggemaakt omdat er veroverd wordt
        assertEquals(0, pitCollectie[11].getAantalStenen());
        //score pit nummer 14 moet 17 stenen bevatten (start stenen = 14 + 3 door uitgevoerde beurt)
        assertEquals(14, pitCollectie[6].getAantalStenen());

        gameManager.verwijderSpel(game,speler1,speler2);
    }

    @Test
    public void testVeroverNaBigPit() {

        /**
        De test controleert meerdere regels met een verovering na een grote/score pit die moet worden overgeslagen

        1) de laatste steen kan niet in de score pit van de tegenstander terecht komen
        2) de laatste steen verovert de pit tegenover als die van de tegenstander is en de laatste pit leeg was
        3) de score pit wordt correct bijgewerkt
        4) de veroverde pits worden leeggemaakt

        speler2 kiest pit 12, die bevat 9 stenen
        score pit nummer 14 moet 17 stenen bevatten (start stenen = 14 + 3 door uitgevoerde beurt)
        1 steen ingelegd in eigen score pit,
        1 steen veroverd van tegenstanderd uit pit 6 (die pit start met 0 stenen, maar de tegenstanderd legt er zelf 1 steen in)
        1 steen veroverd uit eigen pit omdat die leeg was toen de laatste eigen steen erin gelegd werd
        pit 8 en 6 bevatten na verovering 0 stenen
        */

        //stel spel situatie waarbij pit veroverd kan worden
        //speler 1 pits
        pitCollectie[0] = new Pit(1,2);
        pitCollectie[1] = new Pit(2,0);
        pitCollectie[2] = new Pit(3,11);
        pitCollectie[3] = new Pit(4,9);
        pitCollectie[4] = new Pit(5,0);
        pitCollectie[5] = new Pit(6,0);
        //score pit speler 1
        pitCollectie[6] = new Pit(7,4);

        //speler 2 pits
        pitCollectie[7] = new Pit(8,0);
        pitCollectie[8] = new Pit(9,3);
        pitCollectie[9] = new Pit(10,11);
        pitCollectie[10] = new Pit(11,9);
        pitCollectie[11] = new Pit(12,9);
        pitCollectie[12] = new Pit(13,0);
        //score pit speler 2
        pitCollectie[13] = new Pit(14,14);

        gameManager.maakSpel(spelerList, pitCollectie);

        List<Game> games = gameManager.getGames();
        game = games.get(0);

        //speler2 is aan zet
        speler1.setAanDeBeurt(false);
        speler2.setAanDeBeurt(true);

        //speler2 kiest pit 12
        game.behandelSpelerZet(12, "test-sessie_2");

        //pit 8 moet waarde 0 bevatten, die wordt leeggemaakt omdat er veroverd wordt
        assertEquals(0, pitCollectie[7].getAantalStenen());
        //pit 6 moet waarde 0 bevatten, die wordt leeggemaakt omdat er veroverd wordt
        assertEquals(0, pitCollectie[5].getAantalStenen());
        //score pit nummer 14 moet 17 stenen bevatten (start stenen = 14 + 3 door uitgevoerde beurt)
        assertEquals(17, pitCollectie[13].getAantalStenen());

        gameManager.verwijderSpel(game,speler1,speler2);
    }


    @Test
    public void testVeroverLegePit() {

        /**
         De test controleert meerdere regels met een verovering die niet geldig is, de verovering van een lege pit moet niet mogelijk zijn

        1) de laatste steen verovert de pit tegenover als die van de tegenstander is en de laatste pit leeg was
        2) de tegenovergestelde pit moet wel stenen bevatten, anders gaat de verovering niet door
        3) de score pit blijft ongewijzigd

        speler1 kiest pit 1, die bevat 1 steen
        1 steen ingelegd in pit 2, pit 2 was leeg en bevat nu de laatste steen
        de tegenovergestelde pit 12 is leeg
        score pit moet ongewijzigd blijven
        */

        //stel spel situatie waarbij pit veroverd kan worden
        //speler 1 pits
        pitCollectie[0] = new Pit(1,1);
        pitCollectie[1] = new Pit(2,0);
        pitCollectie[2] = new Pit(3,11);
        pitCollectie[3] = new Pit(4,9);
        pitCollectie[4] = new Pit(5,1);
        pitCollectie[5] = new Pit(6,0);
        //score pit speler 1
        pitCollectie[6] = new Pit(7,4);

        //speler 2 pits
        pitCollectie[7] = new Pit(8,0);
        pitCollectie[8] = new Pit(9,3);
        pitCollectie[9] = new Pit(10,11);
        pitCollectie[10] = new Pit(11,9);
        pitCollectie[11] = new Pit(12,0);
        pitCollectie[12] = new Pit(13,9);
        //score pit speler 2
        pitCollectie[13] = new Pit(14,14);

        gameManager.maakSpel(spelerList,pitCollectie);

        List<Game> games = gameManager.getGames();
        game = games.get(0);

        //speler2 is aan zet
        speler1.setAanDeBeurt(true);
        speler2.setAanDeBeurt(false);

        //speler2 kiest pit 12
        game.behandelSpelerZet(1, "test-sessie_1");

        //pit 8 moet waarde 0 bevatten, die wordt leeggemaakt omdat er veroverd wordt
        assertEquals(1, pitCollectie[1].getAantalStenen());
        //score pit nummer 7 moet 4 stenen bevatten (start stenen = 14 + 3 door uitgevoerde beurt)
        assertEquals(4, pitCollectie[6].getAantalStenen());

        gameManager.verwijderSpel(game,speler1,speler2);
    }
}
