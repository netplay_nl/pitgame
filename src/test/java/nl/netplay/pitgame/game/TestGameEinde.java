package nl.netplay.pitgame.game;

import nl.netplay.pitgame.server.GameManager;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by leon on 22-9-2015.
 *
 * Test het einde van het spel
 */
public class TestGameEinde {
    private GameManager gameManager;
    private Speler speler1;
    private Speler speler2;
    private List<Speler> spelerList= new ArrayList<Speler>();
    private Pit[] pitCollectie = new Pit[14];

    @Before
    public void setUp() {

        speler1 = new Speler("speler1", "test-sessie_1");
        speler2 = new Speler("speler2", "test-sessie_2");
        spelerList.add(speler1);
        spelerList.add(speler2);
        gameManager = GameManager.getInstance();
    }

    @Test
    public void testSpelTenEinde() {

        /**
         De test controleert of het spel ten einde is
         Test het spel met een situatie waarbij na een zet speler 1 geen stenen meer heeft om mee te spelen

         pit nummer 6 bevat de laatste steen die in de score pit van speler 1 wordt gezet,
         hierna heeft speler 1 geen stenen meer om mee te spelen en is het spel ten einde
         */

        //speler 1 pits
        pitCollectie[0] = new Pit(1,0);
        pitCollectie[1] = new Pit(2,0);
        pitCollectie[2] = new Pit(3,0);
        pitCollectie[3] = new Pit(4,0);
        pitCollectie[4] = new Pit(5,0);
        pitCollectie[5] = new Pit(6,1);
        //score pit speler 1
        pitCollectie[6] = new Pit(7,16);

        //speler 2 pits
        pitCollectie[7] = new Pit(8,9);
        pitCollectie[8] = new Pit(9,3);
        pitCollectie[9] = new Pit(10,11);
        pitCollectie[10] = new Pit(11,9);
        pitCollectie[11] = new Pit(12,9);
        pitCollectie[12] = new Pit(13,0);
        //score pit speler 2
        pitCollectie[13] = new Pit(14,14);

        gameManager.maakSpel(spelerList,pitCollectie);

        List<Game> games = gameManager.getGames();
        Game game = games.get(0);

        //speler2 is aan zet
        speler1.setAanDeBeurt(true);
        speler2.setAanDeBeurt(false);

        //speler1 kiest pit 6
        game.behandelSpelerZet(6, "test-sessie_1");

        //pit 6 moet waarde 17 bevatten na het toevoegen van de laatste steen
        assertEquals(17, pitCollectie[6].getAantalStenen());
        //pit 14 moet waarde 55 bevatten na het toevoegen van alle overgebleven stenen
        assertEquals(55, pitCollectie[13].getAantalStenen());
        //het spel moet ten einde zijn en dus niet meer bestaan
        assertEquals(0, gameManager.getGames().size());
    }

    @Test
    public void testSpelNietTenEinde() {

        /**
         De test controleert of het spel NIET ten einde is
         Test het spel met een situatie waarbij na een zet speler 1 geen stenen meer heeft om mee te spelen

         pit nummer 6 bevat de laatste steen die in de score pit van speler 1 wordt gezet,
         hierna heeft speler 1 geen stenen meer om mee te spelen en is het spel ten einde
         */

        //speler 1 pits
        pitCollectie[0] = new Pit(1,0);
        pitCollectie[1] = new Pit(2,0);
        pitCollectie[2] = new Pit(3,0);
        pitCollectie[3] = new Pit(4,0);
        pitCollectie[4] = new Pit(5,1);
        pitCollectie[5] = new Pit(6,0);
        //score pit speler 1
        pitCollectie[6] = new Pit(7,15);

        //speler 2 pits
        pitCollectie[7] = new Pit(8,0);
        pitCollectie[8] = new Pit(9,9);
        pitCollectie[9] = new Pit(10,11);
        pitCollectie[10] = new Pit(11,9);
        pitCollectie[11] = new Pit(12,9);
        pitCollectie[12] = new Pit(13,0);
        //score pit speler 2
        pitCollectie[13] = new Pit(14,14);

        gameManager.maakSpel(spelerList,pitCollectie);

        List<Game> games = gameManager.getGames();
        Game game = games.get(0);

        //speler2 is aan zet
        speler1.setAanDeBeurt(true);
        speler2.setAanDeBeurt(false);

        //speler1 kiest pit 6
        game.behandelSpelerZet(5, "test-sessie_1");

        //pit 6 moet waarde 17 bevatten na het toevoegen van de laatste steen
        assertEquals(15, pitCollectie[6].getAantalStenen());
        //pit 14 moet waarde 55 bevatten na het toevoegen van alle overgebleven stenen
        assertEquals(14, pitCollectie[13].getAantalStenen());
        //het spel moet nog bestaan
        assertEquals(1, gameManager.getGames().size());

        game.stopSpel(null);

    }
}
