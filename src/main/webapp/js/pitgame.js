/**
 * Created by leon on 17-9-2015.
 *
 * pit game logica
 */
var websocketHost = "ws://netplay.nl:8085/websocket";
//var websocketHost = "ws://localhost:8080/websocket";

$(document).ready(function() {

    var gameId;

    /**
     *  alle speler click events
     */

    //speler klikt: aanmelden
    $("#login").submit(function (event) {
        event.preventDefault();
        stuurLogin(event.data);
    });

    //speler klikt op een pit
    $("span.pit").click(function () {
        stuurZet(this.id.replace("pit", ""), gameId);
    });


    /**
     *  game functionaliteiten
     */

    //stuur een zet op naar de server
    function stuurZet(pitId, gameId) {
        var jsonMessage = {
            "berichtType": "zetBericht",
            "gameData": {
                "pitId": parseInt(pitId,10),
                "gameId": gameId
            }
        };
        webSocket.send(JSON.stringify(jsonMessage));
    }

    //ontvangen van de zet informatie
    function ontvangZet(gameData) {
        plaatsStenen(gameData,true);
    }

    //speler inloggen
    function stuurLogin() {
        var jsonMessage = {
            "berichtType": "loginBericht",
            "gameData": {
                "naam": $("#username").val()
            }
        };
        webSocket.send(JSON.stringify(jsonMessage));
    }

    //plaats de stenen in de pits, toon optioneel de verschillen in de pits na elke zet
    function plaatsStenen(gameData,toonUpdateBox) {
        $.each(gameData, function (key, value) {
            //toon optioneel de zet update van de pit in een box
            if (toonUpdateBox) {
                toonZet(value);
            }
            //plaats de nieuwe waarde in de pit
            $("#pit" + value.pitId).html(value.aantalStenen);
        });
    }

    //toon het verschil met de vorige waarde in de pit
    function toonZet(pit){
        //maak het html element waarin de zet met het verschil van de vorige waarde wordt getoond
        var updatebox = document.createElement('span');
        updatebox.className = "arrow_box_top" ;
        updatebox.id = "box"+pit.pitId;
        if (pit.pitId > 7) { updatebox.className = "arrow_box_bottom";}
        if (pit.pitId == 7) {updatebox.className = "arrow_box_top p2pit";}
        if (pit.pitId == 14) {updatebox.className = "arrow_box_bottom p1pit";}

        var oudeWaarde = $("#pit" + pit.pitId).html();
        var nieuweWaarde = pit.aantalStenen;

        //reken het verschil uit
        var verschil = (nieuweWaarde - oudeWaarde);

        //alleen de infobox tonen als de waarde veranderd is
        if (verschil !== 0){
            if (verschil > 0) {
                verschil = "+"+verschil;
            }
            updatebox.innerHTML = verschil;
            //voeg box toe na de pit
            $("#pit" + pit.pitId).after(updatebox);
            //verwijder de box weer na korte duur
            $("#box"+pit.pitId).delay(1800).queue(function() { $(this).remove(); });
        }
    }

    //start een spel tussen 2 spelers, zet het spelbord op
    function startSpel(gameData) {
        toonMelding("Het spel is begonnen!");
        //aanmelden verbergen
        $("#login").hide();

        //alle pits opzetten
        plaatsStenen(gameData,false);

    }

    //spel einde melding tonen
    function eindeSpel(gameData) {
        //aanmelden scherm tonen
        $("#login").show();

        toonMelding(gameData);
    }

    //toon de naam van speler1 en speler2, toon wie aan de beurt is
    function toonspelerInformatie(gameData) {
        var beurtText = " is aan de beurt.";
        gameId = gameData[0].gameId;

        $("#speler1").html("Speler 1: " + gameData[0].naam);
        $("#speler2").html("Speler 2: " + gameData[1].naam);

        if (gameData[0].aanDeBeurt) {
            $("#speler1").append(beurtText);
        } else {
            $("#speler2").append(beurtText);
        }
    }

    //toon alle aangemelde spelers in een lijst
    function toonSpelersLijst(gameData) {
        var spelerlijst = "";
        $.each(gameData, function (key, value) {
            spelerlijst += value.naam + " ("+value.spelerSpelStatus.toLowerCase().replace(/_/g, ' ')+")<br/>";
        });
        $("#spelerlijst").html(spelerlijst);
    }

    //melding tonen
    function toonMelding(melding) {
        //toon melding
        $("#messages").html(melding);
        $("#messages").show();
        //verberg melding
        $("#messages").delay(10000).fadeOut('slow');

    }

    //verwerk de json berichten van de websocket
    function verwerkWebsocketBericht(event) {
        //console.log("websocket bericht ontvangen: " + event.data);
        var bericht = JSON.parse(event.data);

        if (bericht.berichtType == "spelerlijst") {
            toonSpelersLijst(bericht.gameData);
        } else if (bericht.berichtType == "startSpel") {
            startSpel(bericht.gameData);
        } else if (bericht.berichtType == "eindeSpel") {
            eindeSpel(bericht.gameData);
        } else if (bericht.berichtType == "spelerInfo") {
            toonspelerInformatie(bericht.gameData);
        } else if (bericht.berichtType == "ontvangZet") {
            ontvangZet(bericht.gameData);
        }
    }

    /**
     *  websocket functionaliteit
     */

    var webSocket = new WebSocket(websocketHost);

    webSocket.onerror = function (event) {
        onError(event);
    };

    webSocket.onopen = function (event) {
        onOpen(event);
    };

    webSocket.onmessage = function (event) {
        verwerkWebsocketBericht(event);
    };

    webSocket.onclose = function (event) {
        toonMelding("Verbinding verbroken met gameserver. Meld je opnieuw aan.");
    };

    function onOpen(event) {
        toonMelding("Succesvol verbonden met gameserver!<br/> Meld je aan om een spel te spelen.");
    }

    function onError(event) {
        toonMelding(event.data);
    }
});
   