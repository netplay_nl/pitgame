package nl.netplay.pitgame.game;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by leon on 16-9-2015.
 *
 * De speler, bevat alle informatie spelerinformatie
 */
public class Speler {

    private static final Logger LOGGER = LoggerFactory.getLogger(Speler.class);

    public enum SpelerType {
        SPELER_1, SPELER_2
    }
    public enum SpelerSpelStatus {
        WACHT_OP_SPEL, BEZIG_MET_SPEL
    }
    private final String naam;
    private boolean aanDeBeurt;
    private String gameId;
    @JsonIgnore
    private final String websocketSessionId;

    private SpelerType spelerType;
    private SpelerSpelStatus spelerSpelStatus;

    public Speler(String naam, String websocketSessionId){
        LOGGER.debug("Nieuwe speler {} wordt angemaakt", naam);
        this.naam = naam;
        this.websocketSessionId = websocketSessionId;
        this.spelerSpelStatus = SpelerSpelStatus.WACHT_OP_SPEL;
        this.aanDeBeurt = false;
    }

    public String getWebsocketSessionId() {
        return websocketSessionId;
    }

    @JsonGetter
    public String getNaam() {
        return naam;
    }

    public void setSpelerType(SpelerType spelerType) {
        this.spelerType = spelerType;
    }

    @JsonGetter
    public SpelerType getSpelerType() {
        return spelerType;
    }

    @JsonGetter
    public SpelerSpelStatus getSpelerSpelStatus() {
        return spelerSpelStatus;
    }

    public void setSpelerSpelStatus(SpelerSpelStatus spelerSpelStatus) {
        this.spelerSpelStatus = spelerSpelStatus;
    }

    @JsonGetter
    public boolean isAanDeBeurt() {
        return aanDeBeurt;
    }

    public void setAanDeBeurt(boolean aanDeBeurt) {
        this.aanDeBeurt = aanDeBeurt;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @JsonGetter
    public String getGameId() {
        return gameId;
    }
}
