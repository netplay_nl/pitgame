package nl.netplay.pitgame.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by leon on 16-9-2015.
 *
 * Het spelbord, bevat alle pits.
 *
 * Het spelbord voorziet in het ophalen en verplaatsten van de stenen in de pits
 */
public class Board {

    private final Pit[] pitCollectie;
    private static final Logger LOGGER = LoggerFactory.getLogger(Board.class);

    //maak het spelbord aan en initieer alle pits
    public Board() {
        LOGGER.debug("Spelbord wordt aangemaakt...");

        pitCollectie = new Pit[14];

        //pits speler 1
        pitCollectie[0] = new Pit(1,6);
        pitCollectie[1] = new Pit(2,6);
        pitCollectie[2] = new Pit(3,6);
        pitCollectie[3] = new Pit(4,6);
        pitCollectie[4] = new Pit(5,6);
        pitCollectie[5] = new Pit(6,6);
        //score pit speler 1
        pitCollectie[6] = new Pit(7,0);

        //pits speler 2
        pitCollectie[7] = new Pit(8,6);
        pitCollectie[8] = new Pit(9,6);
        pitCollectie[9] = new Pit(10,6);
        pitCollectie[10] = new Pit(11,6);
        pitCollectie[11] = new Pit(12,6);
        pitCollectie[12] = new Pit(13,6);
        //score pit speler 2
        pitCollectie[13] = new Pit(14,0);
    }

    //maak custom spelbord aan en initeer alle pits
    protected Board(Pit [] pit) {
        this.pitCollectie = pit;
    }

    protected Pit[] getPitCollectie() {
        return this.pitCollectie;
    }

    /**
    /* Haal het pitnummer van de tegenovergestelde pit op. Het totaal aantal gebruikte pits minus de geselecteerde pit: 14 - pitNummer.
    * @param pitNummer pitnummer waarvan de tegenovergestelde pit moet worden opgehaald
    * @return het tegenovergestelde pitnummer
    */
    protected int getTegenOverGesteldePit(int pitNummer){
        return 14 - pitNummer;
    }

    protected int getAantalStenenVanPit(int pitNummer){
        //corrigen met -1 omdat we niet vanaf 0 nummer met de pitId's
        return this.pitCollectie[pitNummer-1].getAantalStenen();
    }

    //haal het aantal stenen op van de speler waarmee nog gespeeld kan worden
    protected int getAantalOvergeblevenStenenVanSpeler(int spelerNummer) {
        int overGeblevenStenenVanSpeler = 0;
        if (spelerNummer == 1){
            int laatstePit = 6;
            for (int pitnummer = 1; pitnummer <= laatstePit; pitnummer++ ) {
                overGeblevenStenenVanSpeler += getAantalStenenVanPit(pitnummer);
            }
        } else {
            //speler 2
            int laatstePit = 13;
            for (int pitnummer = 8; pitnummer <= laatstePit; pitnummer++ ) {
                overGeblevenStenenVanSpeler += getAantalStenenVanPit(pitnummer);
            }
        }

        return overGeblevenStenenVanSpeler;
    }

    protected void leegPit(int pitNummer){
        this.pitCollectie[pitNummer-1].maakLeeg();
    }

    protected void plaatsInPit(int pitNummer, int aantalStenen) {
        this.pitCollectie[pitNummer-1].plaatsSteen(aantalStenen);
    }
}
