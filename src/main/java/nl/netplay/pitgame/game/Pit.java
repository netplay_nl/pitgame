package nl.netplay.pitgame.game;

import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * Created by leon on 16-9-2015.
 *
 * De pit, een stukje speelveld welke stenen bevat
 */
public class Pit {

    private final int pitId;
    private int aantalStenen;

    Pit(int pitId, int aantalStenen) {
        this.pitId = pitId;
        this.aantalStenen = aantalStenen;
    }

    protected void plaatsSteen(int aantal){
        this.aantalStenen = this.aantalStenen + aantal;
    }

    protected void maakLeeg(){
        this.aantalStenen = 0;
    }

    @JsonGetter
    public int getPitId() {
        return pitId;
    }

    @JsonGetter
    public int getAantalStenen() {
        return aantalStenen;
    }

}
