package nl.netplay.pitgame.game;

import nl.netplay.pitgame.server.GameManager;
import nl.netplay.pitgame.server.WebSocketBericht;
import nl.netplay.pitgame.server.WebSocketUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

/**
 * Created by leon on 16-9-2015.
 *
 * Het spel, bevat alle logica om het spel aan te maken en te regelen
 */
public class Game {

    private final Speler speler1;
    private final Speler speler2;
    private final String gameId;
    private final Board spelbord;
    private boolean spelerMagNogEenBeurt = false;

    static final int SCORE_PIT_SPELER_1 = 7;
    static final int SCORE_PIT_SPELER_2 = 14;

    private static final Logger LOGGER = LoggerFactory.getLogger(Game.class);

    /**
     /* Maak het spel tussen 2 spelers en kies de standaard pitlayout
     * @param speler1 speler1 van het spel
     * @param speler2 speler2 van het spel
    */
    public Game(Speler speler1, Speler speler2) {
        this(speler1,speler2,null);
    }

    /**
     /* Maak het spel tussen 2 spelers en kies een custom pitlayout
     * @param speler1 speler1 van het spel
     * @param speler2 speler2 van het spel
     * @param pits optionele aangepaste pits om het spel mee te starten
     */
    public Game(Speler speler1, Speler speler2, Pit[] pits) {
        this.speler1 = speler1;
        this.speler2 = speler2;
        this.speler1.setSpelerType(Speler.SpelerType.SPELER_1);
        this.speler2.setSpelerType(Speler.SpelerType.SPELER_2);
        this.speler1.setSpelerSpelStatus(Speler.SpelerSpelStatus.BEZIG_MET_SPEL);
        this.speler2.setSpelerSpelStatus(Speler.SpelerSpelStatus.BEZIG_MET_SPEL);
        this.speler1.setGameId(speler1.getWebsocketSessionId() + speler2.getWebsocketSessionId());
        this.speler2.setGameId(speler1.getWebsocketSessionId() + speler2.getWebsocketSessionId());
        this.gameId = speler1.getWebsocketSessionId() + speler2.getWebsocketSessionId();

        if (pits == null) {
            spelbord = new Board();
        } else {
            spelbord = new Board(pits);
        }

        startSpel();
    }

    /**
    /* Stop het spel tussen 2 spelers, verijwder de spelers en het spel
     * @param verliezer stuur optioneel de verliezer mee (diegene die het spel vroegtijdig stopt)
     */
    public void stopSpel(Speler verliezer) {

        WebSocketBericht webSocketBericht;
        if (verliezer != null) {
            LOGGER.info("Het spel {} wordt gestopt door speler: {} en verliest daardoor het spel: {}", verliezer.getNaam());
            webSocketBericht = new WebSocketBericht("eindeSpel","Speler "+verliezer.getNaam()+" verliet het spel. Het spel is afgelopen! <br/>Meld je opnieuw aan om nog een spel te spelen.");
        } else {
            LOGGER.info("Het spel {} wordt gestopt tussen speler1: {} en speler2: {}", gameId, speler1.getNaam(),speler2.getNaam());
            webSocketBericht = new WebSocketBericht("eindeSpel","Het spel is afgelopen! <br/>"+speler1.getNaam()+" score:"+spelbord.getAantalStenenVanPit(7)+"<br/> "+ speler2.getNaam() +" score:"+spelbord.getAantalStenenVanPit(14)+"<br/>Meld je opnieuw aan om nog een spel te spelen.");
        }

        GameManager gameManager = GameManager.getInstance();

        //stuur het spel einde bericht
        WebSocketUtility.stuurBericht(speler1, webSocketBericht);
        WebSocketUtility.stuurBericht(speler2, webSocketBericht);

        //verwijder spel en spelers
        gameManager.verwijderSpel(this,speler1,speler2);

    }

    /**
    /* start een spel, start met de standaard 6 stenen per pit, of aangepaste pits
    */
    private void startSpel() {
        LOGGER.info("Het spel tussen speler {} en speler {} wordt gestart.", speler1.getNaam(), speler2.getNaam());

        //stuur naar de 2 spelers dat het spel is gestart en initieer het spelbord voor ze
        stuurPitInformatie(true);

        //stuur naar de spelers wie de tegenstander is en wie er aan de beurt is
        stuurSpelerInformatie(true);

    }

    /**
     /* stuur de pit informatie door naar de 2 spelers
     * @param nieuwSpel geef in het websocket bericht aan of het een nieuw spel betreft
     */
    protected void stuurPitInformatie(boolean nieuwSpel) {

        WebSocketBericht webSocketBericht;
        if (nieuwSpel) {
            webSocketBericht = new WebSocketBericht("startSpel",spelbord.getPitCollectie());
        } else{
            webSocketBericht = new WebSocketBericht("ontvangZet",spelbord.getPitCollectie());
        }
        WebSocketUtility.stuurBericht(speler1, webSocketBericht);
        WebSocketUtility.stuurBericht(speler2, webSocketBericht);

    }


    /**
     /* stuur de namen van beide spelers op en wie aan de beurt is
     * @param random    bepaal of de beurt random moet worden bepaald
     */
    protected void stuurSpelerInformatie(boolean random){
        if (random){
            Random randomStartSpeler = new Random();
            boolean startSpeler1 = randomStartSpeler.nextBoolean();
            if (startSpeler1) {
                speler1.setAanDeBeurt(true);
            } else {
                speler2.setAanDeBeurt(true);
            }
        }

        Speler[] spelerList = new Speler[2];
        spelerList[0] = speler1;
        spelerList[1] = speler2;

        WebSocketBericht webSocketBericht = new WebSocketBericht("spelerInfo",spelerList);
        WebSocketUtility.stuurBericht(speler1, webSocketBericht);
        WebSocketUtility.stuurBericht(speler2, webSocketBericht);

    }

    /**
     /* behandel de zet van de speler
     * pak de stenen het gekozen pitnummer en plaats stenen navolgende pits, volg de regels van het spel op
     * @param geselecteerdePitId    het pit nummer die de speler heeft geselecteerd
     * @param sessionId het sessie id van de speler die de zet uitvoert, nodig voor controle op een geldige zet
     */
    public void behandelSpelerZet(int geselecteerdePitId, String sessionId){

        //steen wordt niet geplaatst in de grote pit van de tegenstander
        int pitOverslaanPitId = 0;
        if (speler1.isAanDeBeurt()) {
            pitOverslaanPitId = SCORE_PIT_SPELER_2;
        }
        if (speler2.isAanDeBeurt()) {
            pitOverslaanPitId = SCORE_PIT_SPELER_1;
        }

        //indien de zet geldig is, voer deze uit
        if (controleerZetGeldigheid(geselecteerdePitId,sessionId)) {

            //standaard mag de speler 1 keer
            spelerMagNogEenBeurt = false;

            //haal het aantal stenen op van de door de speler geselecteerde pit
            int aantalStenen = spelbord.getAantalStenenVanPit(geselecteerdePitId);

            //verwijder alle stenen uit de geselecteerde pit
            spelbord.leegPit(geselecteerdePitId);

            //zolang er nog stenen zijn, plaats de stenen in de volgende pits en pas de regels toe
            zaaiStenen(geselecteerdePitId,aantalStenen,pitOverslaanPitId);

            //hebben de spelers nog stenen om mee te spelen?
            if (isSpelTenEinde()) {
                 //stop dit spel tussen de 2 spelers
                stuurPitInformatie(false);
                stopSpel(null);
            } else {
                //stuur naar de 2 spelers de geupdate pits
                stuurPitInformatie(false);

                bepaalSpelerBeurt(spelerMagNogEenBeurt, geselecteerdePitId);

            }
        }
    }

    /**
     /* leg alle stenen in de volgende pits vanaf de geselecteerde pit en pas alle regels toe bij het zaaien
     * @param geselecteerdePitId    het pit nummer die de speler heeft geselecteerd
     * @param aantalStenen het aantal stenen dat is opgepakt
     * @param pitOverslaanPitId de score pit die moet worden overgeslagen, de scorepit van de tegenstande, hier komt geen steen in
     */
    private void zaaiStenen(int geselecteerdePitId, int aantalStenen, int pitOverslaanPitId) {

        int aantalStenenOver = aantalStenen;

        boolean startVerplaatsen = false;

        while (aantalStenenOver != 0) {
            //loop door alle pits heen totdat de stenen op zijn, start vanaf de pit na de geselecteerde pit


            //loop door alle pits heen totdat de stenen op zijn
            for (Pit pit : this.spelbord.getPitCollectie()) {

                //start het verplaatsen als de geslecteerde pit is bereikt en er nog stenen zijn en als dit niet de pit is die overgeslagen moet worden
                if (startVerplaatsen && aantalStenenOver > 0 && pit.getPitId() != pitOverslaanPitId) {
                    aantalStenenOver--;

                    //het plaatsen van de laatste steen
                    if (aantalStenenOver == 0) {
                        //check of de speler nog eens mag
                        spelerMagNogEenBeurt = bepaalLaatsteSteenBeurt(pit.getPitId());
                        //check of de laatste steen een verovering is
                        behandelLaatsteSteenVerovering(pit.getPitId());
                    }
                    //dit is niet de laatste steen, plaats steen zonder andere acties uit te voeren
                    else {
                        spelbord.plaatsInPit(pit.getPitId(),1);
                    }
                }

                //start met stenen verplaatsen vanaf de geselecteerde pit
                if (pit.getPitId() == geselecteerdePitId) {
                    //de geselecteerde pit is gevonden, start vanaf nu met het zaaien van de stenen in de pits na de geselecteerde pit
                    startVerplaatsen = true;
                }
            }
        }
    }

    /**
     /* controleer wie er de aan de beurt is als de laatste steen wordt gelegd
     * als de laatste steen in de eigen score pit land, is de speler nog eens aan de beurt
     * @param huidigPitId    het pit nummer van de pit die verovert gaat worden (de tegenovergestelde pit waarin de laatste steen eindigde)
     * @return  boolean ja/nee of de huidige speler nogmaals mag
     */
    private boolean bepaalLaatsteSteenBeurt(int huidigPitId){
        //als de laatste steen in de eigen score pit land, mag de speler nog een keer
        if (speler1.isAanDeBeurt() && huidigPitId == SCORE_PIT_SPELER_1) {
            return true;
        } else if (speler2.isAanDeBeurt() && huidigPitId == SCORE_PIT_SPELER_2) {
            return true;
        }
        return false;
    }

    /**
     /* controleer of de laatste steen in een eigen lege pit land
     * als de laatste steen in de eigen lege pit land, verovert de speler de pit van de tegenstander als deze niet leeg is
     * @param huidigPitId    het pit nummer van de pit die verovert gaat worden (de tegenovergestelde pit waarin de laatste steen eindigde)
     */
    private void behandelLaatsteSteenVerovering(int huidigPitId){

        //verover tegenovergesteld pit als speler 1 aan de beurt is en het zijn eigen lege pit is
        if (spelbord.getAantalStenenVanPit(huidigPitId) == 0 && huidigPitId < SCORE_PIT_SPELER_1 && speler1.isAanDeBeurt()) {
            veroverPit(huidigPitId, SCORE_PIT_SPELER_1);
        }
        //verover tegenovergesteld pit als speler 2 aan de beurt is en het zijn eigen lege pit is
        else if (spelbord.getAantalStenenVanPit(huidigPitId) == 0 && huidigPitId > SCORE_PIT_SPELER_1 && huidigPitId != SCORE_PIT_SPELER_2 && speler2.isAanDeBeurt()) {
            veroverPit(huidigPitId, SCORE_PIT_SPELER_2);
        }
        //de laatste steen komt in een pit die niet leeg is, plaats de steen zonder andere acties uit te voeren
        else {
            spelbord.plaatsInPit(huidigPitId,1);
        }
    }

    /**
     /* functie om een pit veroveren
     * @param huidigPitId    het pit nummer van de pit die verovert gaat worden (de tegenovergestelde pit waarin de laatste steen eindigde)
     * @param scorePitId de score pit waarin de veroverde stenen worden geplaatst
     */
    private void veroverPit(int huidigPitId, int scorePitId){
        //bepaal tegenovergestelde pit
        int tegenOverGesteldePitId = spelbord.getTegenOverGesteldePit(huidigPitId);
        //pak stenen in tegenovergesteld pit en tel er 1 bij op van de steen die de speler zelf plaatst in de eigen pit
        int aantalVeroverdeStenen = spelbord.getAantalStenenVanPit(tegenOverGesteldePitId);
        //contrleer het aantal stenen in tegenoverstelde pit: lege tegenovergestelde pit wordt niet veroverd
        if (aantalVeroverdeStenen >0) {
            //verwijder alle stenen de laatste pit en veroverde pit
            spelbord.leegPit(huidigPitId);
            spelbord.leegPit(tegenOverGesteldePitId);
            //plaats veroverde stenen in grote pit van speler 1 plus de steen zelf
            spelbord.plaatsInPit(scorePitId, aantalVeroverdeStenen + 1);
        } else {
            //er wordt niet veroverd, plaats gewoon 1 steen in de laatste pit
            spelbord.plaatsInPit(huidigPitId,1);
        }
    }

    /**
     * Controleer of de spelers nog stenen hebben om mee te spelen,
     * als 1 van de 2 spelers geen stenen meer heeft dan is het spel te einde
     * @return ja/nee einde spel
     */
    private boolean isSpelTenEinde() {
        //controleer voor speler 1 of pits 1-6 leeg zijn en controleer of voor speler 2 of pits 8-13 leeg zijn
        if (spelbord.getAantalOvergeblevenStenenVanSpeler(1) == 0){
            //verplaatst overige overgebleven stenen van speler 1 of speler 2 in de corresponderende score pit
            spelbord.plaatsInPit(SCORE_PIT_SPELER_2,spelbord.getAantalOvergeblevenStenenVanSpeler(2));
            return true;
        } else if (spelbord.getAantalOvergeblevenStenenVanSpeler(2) == 0) {
            //verplaatst overige overgebleven stenen van speler 1 of speler 2 in de corresponderende score pit
            spelbord.plaatsInPit(SCORE_PIT_SPELER_1, spelbord.getAantalOvergeblevenStenenVanSpeler(1));
            return true;
        }
        //het spel mag door
        return false;
    }

    /**
     /* controleer of de geselecteerde pit geselecteerd werd door de speler die aan de beurt is
     * @param geselecteerdePitId    het pit nummer van de geselecteerde pit
     * @param sessionId sessie van de speler waarop gecontroleerd wordt
     * @return boolean geldige zet ja of nee
     */
    private boolean controleerZetGeldigheid(int geselecteerdePitId, String sessionId){

        //bepaal of de speler aan de beurt is en op een pitnummer heeft geklikt die van de speler is
        //bepaal of de zet afkomstig is van de speler die aan de beurt is adhv het sessionid en of de aangeklikte pit stenen bevat
        if (speler1.isAanDeBeurt() && geselecteerdePitId < 7 && speler1.getWebsocketSessionId().equals(sessionId) && spelbord.getAantalStenenVanPit(geselecteerdePitId) > 0) {
            return true;
        }
        //bepaal of de speler aan de beurt is en op een pitnummer heeft geklikt die van de speler is
        //bepaal of de zet afkomstig is van de speler die aan de beurt is adhv het sessionid en of de aangeklikte pit stenen bevat
        else if (speler2.isAanDeBeurt() && geselecteerdePitId > 7 && speler2.getWebsocketSessionId().equals(sessionId) && spelbord.getAantalStenenVanPit(geselecteerdePitId) > 0) {
                return true;
        }else {
            LOGGER.debug("ongeldige zet, speler speelt voor zijn beurt :)");
        }
        return false;
    }

    /**
     /* bepaal adhv uitgevoerde regels welke speler er aan de beurt is en communiceer dit
     * @param spelerMagNogEenBeurt mag de speler
     * @param geselecteerdePitId    het pit nummer van de geselecteerde pit
     */
    //
    private void bepaalSpelerBeurt(boolean spelerMagNogEenBeurt,int geselecteerdePitId){
        //bepaal wie aan de beurt is na deze zet
        if (!spelerMagNogEenBeurt && speler1.isAanDeBeurt() && geselecteerdePitId < 7) {
            speler1.setAanDeBeurt(false);
            speler2.setAanDeBeurt(true);
        } else if (!spelerMagNogEenBeurt && speler2.isAanDeBeurt() && geselecteerdePitId > 7) {
            speler2.setAanDeBeurt(false);
            speler1.setAanDeBeurt(true);
        }

        //stuur naar de 2 spelers wie er aan de beurt is
        stuurSpelerInformatie(false);
    }

    public String getGameId() {
        return gameId;
    }
}
