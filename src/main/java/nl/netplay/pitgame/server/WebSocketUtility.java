package nl.netplay.pitgame.server;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import nl.netplay.pitgame.game.Speler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.Session;
import java.io.IOException;

/**
 * Created by leon on 11-10-2015.
 *
 * Websocket Utility class om berichten uit te wisselen tussen client en server. Zet tevens json om naar pojo WebsocketBericht
 */
public final class WebSocketUtility {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketUtility.class);

    private WebSocketUtility(){
        //niet aan te maken
    }

    /**
     * verstuur het bericht naar de speler via websocket
     * @param speler de speler (sessie) die het bericht gaat ontvangen
     * @param bericht het websocket bericht (json) bericht naar de browser gestuurd
     */
    public static void stuurBericht(Speler speler, WebSocketBericht bericht) {

        LOGGER.debug("Sending json: {}", bericht.toString());

        //pojo naar json
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = null;
        try {
            json = ow.writeValueAsString(bericht);
        } catch (JsonProcessingException e) {
            LOGGER.error("stuurBericht naar speler error" + e);
        }

        if (GameManager.getSessies()!= null && GameManager.getSessies().get(speler.getWebsocketSessionId()) != null) {
            try {
                GameManager.getSessies().get(speler.getWebsocketSessionId()).getBasicRemote().sendText(json);
            } catch (IOException e) {
                LOGGER.error("stuurBericht naar speler error, bericht {}" + e, json);
            }
        }
    }

    /**
     * verstuur het bericht naar een niet speler via websocket
     * @param bericht het websocket bericht (json) bericht naar de browser gestuurd
     * @param sessie de sessie die het bericht gaat ontvangen
     */
    public static void stuurBericht(Session sessie, WebSocketBericht bericht) {

        LOGGER.debug("Sending json: {}", bericht.toString());

        //pojo naar json
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = null;
        try {
            json = ow.writeValueAsString(bericht);
        } catch (JsonProcessingException e) {
            LOGGER.error("Json kan niet omgezet worden error, bericht {}" + e,bericht);
        }

        try {
            sessie.getBasicRemote().sendText(json);
        } catch (IOException e) {
            LOGGER.error("stuurBericht naar sessie error, bericht {}" + e,json);
        }

    }

    /**
     * het onvangen json bericht omzetten naar pojo WebSocketBericht
     * @param jsonMessage het websocket bericht (json String) bericht dat de client naar de server stuurt
     */
    public static WebSocketBericht ontvangBericht(String jsonMessage) {
        //json omzetten naar pojo bericht
        try {
            return new ObjectMapper().readValue(jsonMessage, WebSocketBericht.class);
        } catch (IOException e) {
            LOGGER.error("Kan json bericht niet omzetten naar pojo "+e);
        }
        return null;
    }
}
