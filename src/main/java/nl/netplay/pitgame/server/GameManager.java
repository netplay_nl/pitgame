package nl.netplay.pitgame.server;

import nl.netplay.pitgame.game.Game;
import nl.netplay.pitgame.game.Pit;
import nl.netplay.pitgame.game.Speler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.Session;
import java.io.IOException;
import java.util.*;

/**
 * Created by leon on 16-9-2015.
 *
 * Game manager maakt de spellen adv de aangemeld spelers en bevat de logica om de game communicatie te verwerken
 */
public class GameManager {

    private static List<Speler> spelers = new ArrayList<>();
    private static List<Game> games = new ArrayList<>();
    private static Map<String, Session> sessies = new HashMap<>(); //sessieid, weboscket sessie

    private static final Logger LOGGER = LoggerFactory.getLogger(GameManager.class);

    //singleton
    private static GameManager instance = null;
    private GameManager() {
        // aanmaken niet mogelijk
    }
    public static GameManager getInstance() {
        if(instance == null) {
            instance = new GameManager();
        }
        return instance;
    }

    /**
     * voeg een sessie toe aan de sessielijst, alle clients staan hier in, ook niet spelers
     * @param session de sessie die toegevoegd wordt
     */
    public void voegSessieToe(Session session) {
        //voeg sessie toe aan lijst
        sessies.put(session.getId(), session);
    }

    /**
     * verwijder een sessie uit de sessielijst, deze sessie heeft geen connectie meer met de server
     * @param session de sessie die verwijdert wordt
     */
    public void verwijderSessie(Session session) {
        sessies.remove(session.getId());
    }

    public static Map<String, Session> getSessies() {
        return sessies;
    }

    /**
     * toon de spelerlijst aan iedere sessie op de website
     */
    public void toonSpelers(){

        //verstuur de complete spelerslijst
        WebSocketBericht webSocketBericht = new WebSocketBericht("spelerlijst",spelers);

        for (Map.Entry<String, Session> entry : sessies.entrySet()) {
            WebSocketUtility.stuurBericht(entry.getValue(), webSocketBericht);
        }
    }

    /**
     * Voeg een speler toe aan de spelerlijst die op de website toont
     * @param speler de speler die wordt toegevoegd
     */
    public void voegSpelerToe(Speler speler) {
        spelers.add(speler);

        //na toevoegen de spelerlijst updaten voor iedereen
        toonSpelers();

        //na toevoegen kijken of er een game kunnen starten tussen spelers die nog niet spelen
        maakSpel(spelers);
    }

    /**
     * Verwijder speler uit spelerlijst/sessielijst
     * @param sessionId de sessie van de speler die wordt verwijderd
     */
    private void verwijderSpeler(String sessionId) {

        for (ListIterator<Speler> iter = spelers.listIterator(); iter.hasNext(); ) {
            Speler element = iter.next();
            if (element.getWebsocketSessionId().equals(sessionId)) {
                iter.remove();
                //verwijder alleen de speler, niet de sessie
                spelers.remove(element);
            }
        }
        toonSpelers();
    }

    /**
     * Kijk in de spelerslijst of er genoeg spelers beschikbaar zijn om samen een spel te starten
     * @param spelers de spelerslijst
     */
    public void maakSpel(List<Speler> spelers){
        maakSpel(spelers,null);
    }

    /**
     * Kijk in de spelerslijst of er genoeg spelers beschikbaar zijn om samen een spel te starten
     * @param spelers de spelerslijst
     * @param pits geef andere pit start waarden op
     */
    public void maakSpel(List<Speler> spelers,Pit[] pits){
        List<Speler> spelerGameLijst = new ArrayList<>();
        for (Speler speler: spelers) {
            if (speler.getSpelerSpelStatus() == Speler.SpelerSpelStatus.WACHT_OP_SPEL){
                spelerGameLijst.add(speler);
            }
            if (spelerGameLijst.size() == 2) {
                LOGGER.info("Een spel tussen 2 spelers zal worden aangemaakt...");
                Game game = new Game(spelerGameLijst.get(0),spelerGameLijst.get(1),pits);
                games.add(game);
                toonSpelers();
            }
        }
    }

    /**
     * verwijder het spel en verwijder de spelers uit de lijsten
     * @param game het te verwijderen spel
     * @param speler1 speler1 die verwijderd wordt
     * @param speler2 speler2 die verwijderd wordt
     */
    public void verwijderSpel(Game game, Speler speler1, Speler speler2) {
        //verwijder de spelers
        verwijderSpeler(speler1.getWebsocketSessionId());
        verwijderSpeler(speler2.getWebsocketSessionId());
        //verwijder het spel
        games.remove(game);
        //toon de nieuwe spelerlijst (aan de nu nog aangemelde spelers)
        toonSpelers();
    }

    /**
     * verwijder het spel op basis van een sessie (als 1 speler halverwege stop om wat voor reden dan ook)
     * @param session de sessie die afgebroken wordt door een error of het sluiten van de verbinding
     */
    public void breekSpelAf(Session session) {
        String gameId = null;
        Speler verliezer = null;
        //welk spel speelde deze speler?
        for (ListIterator<Speler> iter = spelers.listIterator(); iter.hasNext(); ) {
            Speler element = iter.next();
            //als deze sessie een spel speelde, stop en verwijder het spel
            if (element.getWebsocketSessionId().equals(session.getId())) {
                verliezer = element;
                gameId = element.getGameId();
            }
        }
        //stop spel, stuur spel bericht einde naar overgebleven speler, verijwder spel
        getGame(gameId).stopSpel(verliezer);

        //verwijder de sessie
        verwijderSessie(session);
    }

    /**
     * verwerk het ontvangende berichtenverkeer voor het spel, zet een taak uit
     * @param bericht het omgezette json naar pojo WebSocketBericht vanuit de client/browser gestuurd
     * @param session de sessie die waar het bericht van afkomstig is
     */
    public void verwerkGameBericht(WebSocketBericht bericht, Session session) throws IOException {

        //bericht op inhoud afhandelen
        if ("loginBericht".equals(bericht.getBerichtType())) {
            LOGGER.info("Login bericht ontvangen {}", bericht.toString());

            if (bericht.getGameData() instanceof LinkedHashMap){
                //maak speler
                Speler speler = new Speler((String) ((LinkedHashMap) bericht.getGameData()).get("naam"), session.getId());

                //voeg speler toe aan gameManager
                voegSpelerToe(speler);
            }

        } else if ("zetBericht".equals(bericht.getBerichtType())){
            LOGGER.info("Speler zet ontvangen {}", bericht);
            for (Game game: games){
                if (game.getGameId().equals(((LinkedHashMap) bericht.getGameData()).get("gameId"))){
                    game.behandelSpelerZet((Integer) ((LinkedHashMap) bericht.getGameData()).get("pitId"), session.getId());
                }
            }
        } else {
            LOGGER.error("Ongeldig json websocket bericht ontvangen: {}", bericht);
        }
    }

    /**
     * Haal de lijst met spellen op
     * @return List<Game> games lijst
     */
    public List<Game> getGames() {
        return games;
    }

    /**
     * Haal de lijst met spellen op
     * @return het spel Game
     */
    private Game getGame(String gameId) {
        for (Game game: games){
            if (game.getGameId().equals(gameId)) {
                return game;
            }
        }
        return null;
    }

}
