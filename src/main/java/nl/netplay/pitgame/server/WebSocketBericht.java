package nl.netplay.pitgame.server;

/**
 * Created by nieuwl on 18-9-2015.
 *
 * object waarin alle json data wordt uitgewisseld
 */
public class WebSocketBericht {
    private Object gameData;
    private String berichtType;

    public WebSocketBericht() {
        this.gameData = null;
        this.berichtType = null;
    }

    public WebSocketBericht(String type) {
        this.gameData = null;
        this.berichtType = type;
    }

    public WebSocketBericht(String type, Object data) {
        this.gameData = data;
        this.berichtType = type;
    }

    public Object getGameData(){
        return this.gameData;
    }

    public String getBerichtType(){
        return this.berichtType;
    }

    public void setBerichtType(String berichtType){
        this.berichtType = berichtType;
    }

    public void setGameData(Object gameData){
        this.gameData = gameData;
    }

    @Override
    public String toString() {
        String result = "berichtType = " + getBerichtType() + ", gameData = ";
        if (this.gameData != null) {
            result += this.gameData.toString() + ", berichtType = ";
        } else {
            result += " null, berichtType = ";
        }
        if (this.berichtType != null) {
            result += this.berichtType;
        } else {
            result += " null ";
        }
        return result;
    }
}
