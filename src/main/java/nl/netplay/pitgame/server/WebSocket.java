package nl.netplay.pitgame.server;

/**
 * Created by leon on 16-9-2015.
 *
 * communicatie tussen website en java obv json
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@ServerEndpoint("/websocket")
public class WebSocket {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocket.class);

    @OnMessage
    public void onMessage(String jsonMessage, Session session) {

        //haal gameManager op (of maak de eerste)
        GameManager gameManager = GameManager.getInstance();

        //handel game bericht af
        try {
            gameManager.verwerkGameBericht(WebSocketUtility.ontvangBericht(jsonMessage), session);
        } catch (IOException e) {
            LOGGER.error("websocket bericht niet correct ontvangen "+e);
        }

    }

    @OnOpen
    public void onOpen(Session session) {
        LOGGER.info("Client connected, sessionid: {}", session.getId());

        //haal gameManager op (of maak de eerste)
        GameManager gameManager = GameManager.getInstance();

        //toon spelerlijst
        gameManager.voegSessieToe(session);
        gameManager.toonSpelers();
    }

    @OnClose
    public void onClose(Session session) {
        LOGGER.info("Client connection closed, sessionid: {}", session.getId());

        //haal gameManager op (of maak de eerste)
        GameManager gameManager = GameManager.getInstance();

        //breek het spel af, verwijder de sessie
        gameManager.breekSpelAf(session);
    }

    @OnError
    public void onError(Throwable error, Session session) {
        LOGGER.error("websocket error {}", error.getMessage());

        //haal gameManager op (of maak de eerste)
        GameManager gameManager = GameManager.getInstance();

        //breek het spel af, verwijder de sessie
        gameManager.breekSpelAf(session);
    }

}
